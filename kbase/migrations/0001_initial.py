# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('slug', models.SlugField(max_length=100, verbose_name='Slug')),
                ('created', models.DateTimeField(verbose_name='\u0421\u043e\u0437\u0434\u0430\u043d\u0430', blank=True)),
                ('updated', models.DateTimeField(verbose_name='\u041e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0430', blank=True)),
                ('material_excerpt', models.TextField(verbose_name='\u0412\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u0435 \u043a \u0441\u0442\u0430\u0442\u044c\u0435')),
                ('material', models.TextField(verbose_name='\u0421\u0442\u0430\u0442\u044c\u044f \u043f\u043e\u043b\u043d\u043e\u0441\u0442\u044c\u044e')),
            ],
            options={
                'verbose_name': '\u0421\u0442\u0430\u0442\u044c\u044f',
                'verbose_name_plural': '\u0421\u0442\u0430\u0442\u044c\u0438',
            },
        ),
        migrations.CreateModel(
            name='Prod_Version',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('short_name', models.CharField(max_length=10, verbose_name='\u041a\u0440\u0430\u0442\u043a\u043e\u0435 \u0438\u043c\u044f \u0432\u0435\u0440\u0441\u0438\u0438')),
                ('slug', models.SlugField(max_length=100, verbose_name='Slug')),
                ('short_version_description', models.CharField(max_length=300, verbose_name='\u041a\u0440\u0430\u0442\u043a\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0432\u0435\u0440\u0441\u0438\u0438', blank=True)),
            ],
            options={
                'verbose_name': '\u0412\u0435\u0440\u0441\u0438\u044f \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0430',
                'verbose_name_plural': '\u0412\u0435\u0440\u0441\u0438\u0438 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0430',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('slug', models.SlugField(max_length=100, verbose_name='Slug')),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0430')),
            ],
            options={
                'verbose_name': '\u041f\u0440\u043e\u0434\u0443\u043a\u0442',
                'verbose_name_plural': '\u041f\u0440\u043e\u0434\u0443\u043a\u0442\u044b',
            },
        ),
        migrations.AddField(
            model_name='prod_version',
            name='product',
            field=models.ForeignKey(related_name='versions', verbose_name='\u041f\u0440\u043e\u0434\u0443\u043a\u0442', to='kbase.Product'),
        ),
        migrations.AddField(
            model_name='article',
            name='prod_version',
            field=models.ForeignKey(related_name='articles', verbose_name='\u0412\u0435\u0440\u0441\u0438\u044f', to='kbase.Prod_Version'),
        ),
        migrations.AddField(
            model_name='article',
            name='product',
            field=models.ForeignKey(related_name='articles', verbose_name='\u041f\u0440\u043e\u0434\u0443\u043a\u0442', to='kbase.Product'),
        ),
    ]
