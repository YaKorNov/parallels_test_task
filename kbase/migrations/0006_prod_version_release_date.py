# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('kbase', '0005_auto_20151107_1628'),
    ]

    operations = [
        migrations.AddField(
            model_name='prod_version',
            name='release_date',
            field=models.DateField(default=django.utils.timezone.now, verbose_name='\u0414\u0430\u0442\u0430 \u0440\u0435\u043b\u0438\u0437\u0430'),
        ),
    ]
