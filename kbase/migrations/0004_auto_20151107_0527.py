# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kbase', '0003_auto_20151106_1644'),
    ]

    operations = [
        migrations.AddField(
            model_name='prod_version',
            name='code',
            field=models.CharField(default=b'', max_length=8, verbose_name='\u041a\u043e\u0434 \u0432\u0435\u0440\u0441\u0438\u0438'),
        ),
        migrations.AddField(
            model_name='prod_version',
            name='depth',
            field=models.PositiveIntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='prod_version',
            name='numchild',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='prod_version',
            name='path',
            field=models.CharField(default=1, unique=True, max_length=255),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='prod_version',
            name='short_version_description',
            field=models.TextField(max_length=400, verbose_name='\u041a\u0440\u0430\u0442\u043a\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0432\u0435\u0440\u0441\u0438\u0438', blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='prod_version',
            unique_together=set([('code', 'product')]),
        ),
        migrations.RemoveField(
            model_name='prod_version',
            name='short_name',
        ),
    ]
