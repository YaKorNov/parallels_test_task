# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kbase', '0004_auto_20151107_0527'),
    ]

    operations = [
        migrations.AlterField(
            model_name='prod_version',
            name='code',
            field=models.CharField(default=b'', max_length=100, verbose_name='\u041a\u043e\u0434 \u0432\u0435\u0440\u0441\u0438\u0438'),
        ),
    ]
