# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('kbase', '0002_remove_prod_version_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='prod_version',
            name='short_name',
            field=models.CharField(help_text='\u0423\u043a\u0430\u0436\u0438\u0442\u0435 \u0432\u0435\u0440\u0441\u0438\u044e \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u0430, \u0440\u0430\u0437\u0434\u0435\u043b\u0435\u043d\u043d\u0443\u044e \u0442\u043e\u0447\u043a\u0430\u043c\u0438 - \u043d\u0430\u043f\u0440\u0438\u043c\u0435\u0440,  2.2.1  \u0438\u043b\u0438 7.11.600', max_length=11, verbose_name='\u041a\u0440\u0430\u0442\u043a\u043e\u0435 \u0438\u043c\u044f \u0432\u0435\u0440\u0441\u0438\u0438', validators=[django.core.validators.RegexValidator(regex=b'^\\d{1,3}(\\.\\d{1,3})?(\\.\\d{1,3})?$', message='\u041c\u0430\u043a\u0441\u0438\u043c\u0443\u043c 3 \u0433\u0440\u0443\u043f\u043f\u044b \u0446\u0438\u0444\u0440, \u0440\u0430\u0437\u0434\u0435\u043b\u0435\u043d\u043d\u044b\u0445 \u0442\u043e\u0447\u043a\u0430\u043c\u0438 \u043f\u043e 3 \u0440\u0430\u0437\u0440\u044f\u0434\u0430 \u0432 \u043a\u0430\u0436\u0434\u043e\u0439 \u0433\u0440\u0443\u043f\u043f\u0435 ')]),
        ),
    ]
