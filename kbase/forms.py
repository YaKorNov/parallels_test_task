# -*- coding: utf-8 -*-
from django import forms
from django.forms.models import ErrorList
from ckeditor.widgets import CKEditorWidget
from .models import Product, Prod_Version, Article
from .widgets import ChainedSelectWidget, ChainedSelectWidget_Vers
from treebeard.forms import MoveNodeForm
from django.utils.translation import ugettext_lazy as _
import re
from django.utils.datastructures import SortedDict
import copy




class ProductAdminForm(forms.ModelForm):

    class Meta:
        model = Product
        fields = '__all__'
        widgets = {
            'description': CKEditorWidget(),
        }




class ArticleAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ArticleAdminForm, self).__init__(*args, **kwargs)

        # if kwargs.get('data',None):
        #     obj = Product.objects.get(pk=kwargs['data']['product'])
        #     qs = list(obj.versions.all())
        #     choices = ((None,'---------'), ) + tuple(((qs_obj.pk,qs_obj.short_name) for qs_obj in qs))
        choices = ((None,'---------'), )
        if kwargs.get('instance',None):
            choices+= kwargs['instance'].product.get_list_of_versions()
        params = {'choices':choices}

        self.fields['prod_version'].widget = ChainedSelectWidget(**params)


    def clean(self):

        choices = ((None,'---------'), )
        if not self.cleaned_data.get('prod_version') and self.cleaned_data.get('product'):
            choices+= self.cleaned_data.get('product').get_list_of_versions()

        self.fields['prod_version'].widget.choices = choices

        return self.cleaned_data


    class Meta:
        model = Article
        fields = ('title','slug', 'product', 'prod_version', 'keywords', 'material_excerpt', 'material', )
        widgets = {
            'material': CKEditorWidget(),
        }



class Product_Ver_Add_Form(MoveNodeForm):

    __position_choices_sorted = (
        ('sorted-child', _('Child of')),
    )

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, error_class=ErrorList, label_suffix=':',
                 empty_permitted=False, instance=None):

        super(Product_Ver_Add_Form, self).__init__(
            data, files, auto_id, prefix, initial, error_class, label_suffix,
            empty_permitted, instance)

        opts = self._meta
        if not opts.model.__name__ == 'Prod_Version':
            raise ValueError('Form for product version')

        self.fields['_position'].choices = self.__position_choices_sorted

        choices = self.mk_dropdown_tree(opts.model, for_node=instance)
        params = {'choices':choices,}
        self.fields['_ref_node_id'].widget = ChainedSelectWidget_Vers(**params)

        self.fields['short_version_description'].widget = CKEditorWidget()


    @classmethod
    def tree_choices(cls, choices, product):
        results = list([node for node in Prod_Version.get_root_nodes() if node.product.pk == product.pk])
        for node in results:
            MoveNodeForm.add_subtree(None, node, choices)
        return choices


    def clean(self):

        if  self.cleaned_data.get('product'):
            choices = [(0, _('-- root --'))]
            choices = Product_Ver_Add_Form.tree_choices(choices, self.cleaned_data.get('product'))
        else:
            choices = self.mk_dropdown_tree(Prod_Version, for_node=None)

        self.fields['_ref_node_id'].widget.choices = choices

        #уровень вложенности
        if self.cleaned_data.get('_ref_node_id'):
            parent_object = Prod_Version.objects.get(pk=self.cleaned_data['_ref_node_id'])
            depth = parent_object.get_depth()
            if depth == 3:
                raise forms.ValidationError('Supports only 3 levels depth')
        else:
            depth = 0


        if self.cleaned_data.get('code'):
            if depth==0 and not re.match(r"^[\dA-Za-z-]+$", self.cleaned_data.get('code')):
                raise forms.ValidationError('SubVersion code on level_1 must contain letters or digits')
            if depth==1 and not re.match(r"^\d$", self.cleaned_data.get('code')):
                raise forms.ValidationError('SubVersion code on level_2 must be 1-digit length')
            if depth==2 and not re.match(r"^\d{1,3}$", self.cleaned_data.get('code')):
                raise forms.ValidationError('SubVersion code on level_3 must be 1 to 3-digits length')

        #собираем код
        if self.cleaned_data.get('_ref_node_id') and self.cleaned_data.get('code'):
            if depth==2:
                self.cleaned_data['code'] = (3-len(self.cleaned_data['code'])) * '0' + self.cleaned_data['code']
            self.cleaned_data['code'] = parent_object.code + \
                                            Prod_Version._range_separator + self.cleaned_data['code']

        #проверка на существование версии
        if self.cleaned_data.get('product') and self.cleaned_data.get('code'):
            try:
                Prod_Version.objects.get(code=self.cleaned_data['code'], product=self.cleaned_data['product'])
            except Prod_Version.DoesNotExist:
                pass
            else:
                raise forms.ValidationError('Version with this product already exists')


        return self.cleaned_data




class Product_Ver_Edit_Form(forms.ModelForm):
    class Meta:
        model = Prod_Version
        fields = ('release_date', 'short_version_description', )
        widgets = {
            'short_version_description': CKEditorWidget(),
        }