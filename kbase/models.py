# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.validators import RegexValidator
from treebeard.mp_tree import MP_Node
from django.core.urlresolvers import reverse
from django.utils import timezone



class Product(models.Model):
    title = models.CharField(_(u'Название'), max_length=100, blank=False )
    slug = models.SlugField(_(u'Slug'), max_length=100, blank=False)
    # icon_image = models.ImageField(_(u'Изображение - символ продукта'), upload_to='product_images/', blank=True, null=True )
    description = models.TextField(_(u'Описание продукта'), blank=False, )

    class Meta:
        verbose_name = _(u'Продукт')
        verbose_name_plural = _(u'Продукты')

    def __unicode__(self):
        return u'%s' % (self.title, )

    def get_absolute_url(self):
        return reverse('kbase:products_detail',
                       kwargs={'slug': self.slug})

    def get_list_of_versions(self):
        qs = self.versions.all()
        return tuple(((qs_obj.pk,qs_obj.code) for qs_obj in qs))



class Prod_Version(MP_Node):
    # code = models.CharField(_(u'Код версии'), max_length=8, unique=True, blank=False,
    #                               help_text = u'Укажите версию продукта, разделенную точками - например, '
    #                                                          u' 2.2.1  или 7.11.600',
    #                                              validators=[
    #                                                 RegexValidator(
    #                                                     regex=r'^\d{1,3}(\.\d{1,3})?(\.\d{1,3})?$',
    #                                                     message=_(
    #                                                         u"Максимум 3 группы цифр, разделенных точками по 3 разряда"
    #                                                         u" в каждой группе ")),
    #                                             ])
    product = models.ForeignKey(Product, verbose_name = _(u'Продукт'),
                                     null=False, blank=False, related_name='versions')
    code = models.CharField(_(u'Код версии'), max_length=100, blank=False, default='' )
    release_date = models.DateField(_(u'Дата релиза'), default=timezone.now)
    short_version_description = models.TextField(_(u'Краткое описание версии'), max_length=400, blank=True,)


    _range_separator = '.'
    node_order_by = ['code']

    def get_ancestors_and_self(self):
        return list(self.get_ancestors()) + [self]

    def get_descendants_and_self(self):
        return list(self.get_descendants()) + [self]

    def has_children(self):
        return self.get_num_children() > 0

    def get_num_children(self):
        return self.get_children().count()


    class Meta:
        verbose_name = _(u'Версия продукта')
        verbose_name_plural = _(u'Версии продукта')
        unique_together = (("code", "product"),)

    def __unicode__(self):
        return u'Product %s, v. %s' % (self.product.title, self.code)

    def get_absolute_url(self):
        slug_field = self.code.replace(self._range_separator ,'_')
        return reverse('kbase:product_version_detail',
                       kwargs={'slug': slug_field})




class Article(models.Model):
    title = models.CharField(_(u'Название'), max_length=100, blank=False )
    slug = models.SlugField(_(u'Slug'), max_length=100, blank=False)
    created = models.DateTimeField(_(u'Создана'), blank=True)
    updated = models.DateTimeField(_(u'Обновлена'), blank=True)
    keywords = models.CharField(_(u'Ключевые слова'), max_length=150, blank=True)
    material_excerpt = models.TextField(_(u'Вступление к статье'), blank=False)
    material = models.TextField(_(u'Статья полностью'), blank=False, )
    product = models.ForeignKey(Product, verbose_name = _(u'Продукт'),
                                     null=False, blank=False, related_name='articles')
    prod_version = models.ForeignKey(Prod_Version, verbose_name = _(u'Версия'),
                                     null=False, blank=False, related_name='articles')

    def save(self, *args, **kwargs):
        if not self.pk:
            self.created = timezone.now()
        self.updated = timezone.now()
        super(Article, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _(u'Статья')
        verbose_name_plural = _(u'Статьи')

    def __unicode__(self):
        return u'%s, %s' % (self.title, self.product)

    def get_absolute_url(self):
        return reverse('kbase:article_detail',
                       kwargs={'slug': self.slug})