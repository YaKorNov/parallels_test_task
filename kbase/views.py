# -*- coding: utf-8 -*-
import locale
from django.apps import apps
from django.http import HttpResponse, HttpResponseRedirect
from .models import Product, Prod_Version, Article
from treebeard.forms import MoveNodeForm
import json
from django.views.generic import ListView, DetailView, FormView, TemplateView, \
    CreateView, UpdateView, DeleteView, RedirectView, View
from django.views.generic.detail import SingleObjectMixin
from django.core.urlresolvers import reverse
from django.db.models import Q
from .forms import Product_Ver_Add_Form



class HomeView(TemplateView):
    template_name = 'k_base/home.html'




class ProductsDetailView(DetailView):
    model = Product
    context_object_name = 'product_info'
    template_name = 'k_base/product_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ProductsDetailView, self).get_context_data(**kwargs)
        context['minor_releases'] = list([node for node in Prod_Version.objects.filter(product=self.object).all()
                                          if not node.get_children_count()])
        return context




class ArticleDetailView(DetailView):
    model = Article
    context_object_name = 'article_info'
    template_name = 'k_base/article_detail.html'




class SearchRedirectView(View):

    def post(self, request, *args, **kwargs):
        url = reverse('kbase:search_result_list',
                       kwargs={'search_string': self.request.POST['search_string']})
        return HttpResponseRedirect(url)




class SearchListView(ListView):

    paginate_by = 10
    context_object_name = 'article_list'
    template_name = "k_base/search_list.html"

    def get_context_data(self, **kwargs):
        context = super(SearchListView, self).get_context_data(**kwargs)
        context['search_string'] = self.kwargs.get('search_string')
        return context

    def get_queryset(self):
        search_string = self.kwargs.get('search_string')
        if search_string:
            return Article.objects.filter(Q(title__contains = search_string) | Q(keywords__contains = search_string)).\
                order_by('-updated', '-created')
        return []




class ProductVersionDetailView(SingleObjectMixin, ListView):

    paginate_by = 2
    template_name = "k_base/prod_version_detail.html"
    slug_field = 'code'
    slug_url_kwarg = 'slug'

    def get(self, request, *args, **kwargs):
        if self.kwargs.get(self.slug_url_kwarg):
            self.kwargs[self.slug_url_kwarg] = self.kwargs[self.slug_url_kwarg].replace('_', Prod_Version._range_separator )
        self.object = self.get_object(Prod_Version.objects.all())
        return super(ProductVersionDetailView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ProductVersionDetailView, self).get_context_data(**kwargs)
        context['version_info'] = self.object
        return context

    def get_queryset(self):
        version_list  = self.object.get_descendants_and_self()
        return Article.objects.filter(prod_version__in = version_list).distinct().order_by('-updated', '-created')




def Chained_Selects_View(request, pk):
    obj = Product.objects.get(pk=pk)
    results = list(obj.versions.all())
    final = []
    for item in results:
        final.append({'value': item.pk, 'display': item.code})
    result = json.dumps(final)
    return HttpResponse(result, content_type='application/json')




def Chained_Product_Versions_Selects_View(request,  pk):

    choices = Product_Ver_Add_Form.tree_choices(choices=[], product=Product.objects.get(pk=pk))

    final = []
    for item in choices:
        final.append({'value': item[0], 'display': item[1]})
    result = json.dumps(final)
    return HttpResponse(result, content_type='application/json')