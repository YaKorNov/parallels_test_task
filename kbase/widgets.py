# -*- coding: utf-8 -*-

import django
from django.conf import settings
from django.forms.widgets import Select
from django.utils.safestring import mark_safe


class ChainedSelectsRenderMixin(object):
    def render(self, name, value, attrs=None):
        attrs = dict(self.datas, **{'class': 'chained', 'id': 'id_%s' % name})
        output = super(ChainedSelectsRenderMixin, self).render(name, value, attrs)
        return mark_safe(output)



class ChainedSelectWidget(ChainedSelectsRenderMixin, Select):

    class Media:
        js = ( 'chained_selects/js/jquery.min.js', 'chained_selects/js/chain_selects.js', )

    def __init__(self, choices=(), *args, **kwargs):
        super(ChainedSelectWidget, self).__init__(*args, **kwargs)
        self.datas = {
            'data-parent-id': 'id_product',
            'data-url': '/chained_selects/',
            'data-empty-label': '---------', }
        self.choices = list(choices)




class ChainedSelectWidget_Vers(ChainedSelectsRenderMixin, Select):

    class Media:
        js = ( 'chained_selects/js/jquery.min.js', 'chained_selects/js/chained_prod_version_selects.js', )

    def __init__(self, choices=(), *args, **kwargs):
        super(ChainedSelectWidget_Vers, self).__init__(*args, **kwargs)

        self.datas = {
            'data-parent-id': 'id_product',
            'data-url': '/chained_prod_vers_selects/',
            'data-empty-label': '-- root --', }
        self.choices = list(choices)
