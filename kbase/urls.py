# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from .views import Chained_Selects_View, Chained_Product_Versions_Selects_View, HomeView, ProductVersionDetailView, \
    ArticleDetailView, ProductsDetailView, SearchRedirectView, SearchListView


urlpatterns = patterns('',
    url(r'^$', HomeView.as_view(), name = 'home'),
    url(r'^version/(?P<slug>[\w-]+)/$', ProductVersionDetailView.as_view(), name = 'product_version_detail'),
    url(r'^article/(?P<slug>[\w-]+)/$', ArticleDetailView.as_view(), name = 'article_detail'),
    url(r'^products/(?P<slug>[\w-]+)/$', ProductsDetailView.as_view(), name = 'products_detail'),
    url(r'^search/$', SearchRedirectView.as_view(), name = 'search'),
    url(r'^search_result/(?P<search_string>[\w-]+)/$', SearchListView.as_view(), name = 'search_result_list'),
    url(r'^chained_selects/(?P<pk>\d+)/$', Chained_Selects_View, name='chained_selects'),
    url(r'^chained_prod_vers_selects/(?P<pk>\d+)/$', Chained_Product_Versions_Selects_View, name='chained_prod_vers_selects', ),
)
