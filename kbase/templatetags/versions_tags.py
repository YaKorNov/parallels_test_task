from django import template
from kbase.models import Prod_Version

register = template.Library()


@register.assignment_tag(name="version_tree")
def get_annotated_list(product):
    """
    Gets an annotated list from a tree branch.

    Borrows heavily from treebeard's get_annotated_list
    """
    max_depth = None

    annotated_versions = []

    start_depth, prev_depth = (None, None)

    versions = Prod_Version.get_tree()

    info = {}
    for node in versions:
        node_depth = node.get_depth()
        if start_depth is None:
            start_depth = node_depth
        if max_depth is not None and node_depth > max_depth:
            continue
        if node.product.pk != product.pk:
            continue

        # Update previous node's info
        info['has_children'] = prev_depth is None or node_depth > prev_depth
        if prev_depth is not None and node_depth < prev_depth:
            info['num_to_close'] = list(range(0, prev_depth - node_depth))

        info = {'num_to_close': [],
                'level': node_depth - start_depth}
        annotated_versions.append((node, info,))
        prev_depth = node_depth

    if prev_depth is not None:
        # close last leaf
        info['num_to_close'] = list(range(0, prev_depth - start_depth))
        info['has_children'] = prev_depth > prev_depth

    return annotated_versions
