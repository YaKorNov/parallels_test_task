# -*- coding: utf-8 -*-
from django import forms
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import Product, Prod_Version, Article
from forms import ArticleAdminForm, ProductAdminForm, Product_Ver_Edit_Form, Product_Ver_Add_Form
from treebeard.admin import TreeAdmin
from treebeard.forms import movenodeform_factory




class Prod_VersionAdmin(TreeAdmin):
    list_filter = ('product', )
    def get_form(self, request, obj=None, **kwargs):
        # Proper kwargs are form, fields, exclude, formfield_callback
        if obj: # obj is not None, so this is a change page
            # kwargs['fields'] = ['short_version_description', ]
            kwargs['form'] = Product_Ver_Edit_Form
        else:
            kwargs['form'] = movenodeform_factory(Prod_Version, form = Product_Ver_Add_Form)
        return super(Prod_VersionAdmin, self).get_form(request, obj, **kwargs)


# class ProductVerInline(admin.TabularInline):
#     model = Prod_Version




class ProductAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    form = ProductAdminForm
    # inlines = [
    #     ProductVerInline,
    # ]
    list_display = ('title', )
    list_filter = ('title', )
    search_fields = ('title', )




class ArticleAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    form = ArticleAdminForm
    list_display = ('title', 'created', 'product', )
    list_filter = ('title', 'created', 'product', )
    search_fields = ('title', )



admin.site.register(Prod_Version, Prod_VersionAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Article, ArticleAdmin)