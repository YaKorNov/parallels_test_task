from django.conf import settings
from .models import Product


def products_list_for_sidebar(request):

    return {'product_list_for_sidebar': Product.objects.all().order_by('title')}
