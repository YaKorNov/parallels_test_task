1. Создайте директорию /home/projects/new_project
	sudo mkdir /home/projects/new_project
	sudo chmod 777 -R /home/projects/new_project
2. Перейдите в нее
	cd /home/projects/new_project
2. Убедитесь, что на вашей машине установлен GIT
	sudo apt-get install git
3. Скопируйте исходники проекта с репозитория:
	sudo git clone https://YaKorNov@bitbucket.org/YaKorNov/test_task.git k_base
4. Находясь в текущей диерктории, создайте каталоги для статики и медиа:
	sudo mkdir www
	sudo mkdir www/media
	sudo mkdir www/static_collector
	sudo chmod 777 -R www
5. Убедитесь, что на вашей машине установлен virtualenv:
	sudo pip2 install virtualenv
6. Создайте виртуальное окружение проекта:
	sudo virtualenv -p /usr/bin/python2.7 venv
	sudo chmod 777 -R venv
7. Активируйте окружение:
	source venv/bin/activate
7. Перейдите в папку проекта:
	cd k_base
8. Установите все необходимые зависимости:
	pip2 install -r requirements
9. Соберите статику:
	python manage.py collectstatic
10. Сделайте миграции БД:
	python manage.py migrate
11. Загрузите фикстуры в БД:
	python manage.py loaddata fixtures/data.json
12. Запустите dev-сервер:
	python manage.py runserver 127.0.0.1:8099
13. В браузере перейдите по адресу:
	127.0.0.1:8099
14. Интерфейс администратора доступен по адресу:
	127.0.0.1:8099/admin
	login/ pass
	admin/ 123