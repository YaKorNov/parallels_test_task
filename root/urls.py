
from django.conf.urls import include, url, patterns
from django.contrib import admin
from django.conf import settings


urlpatterns = patterns('',
    url(r'', include('kbase.urls','kbase')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
)

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))
